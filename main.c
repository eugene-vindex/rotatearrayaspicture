#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

typedef struct {
    uint32_t* data;
    size_t N;
} Pic;


uint32_t getPicIndex(const Pic* pic, size_t x, size_t y) {
    size_t N = pic->N;
    assert(x < N && y < N);
    return x + y * N;
}


uint32_t getPicPixel(const Pic* pic, size_t x, size_t y) {
    return pic->data[getPicIndex(pic, x, y)];
}


void printCharPic(const Pic* pic) {
    size_t N = pic->N;
    size_t len = N*N;
    for (size_t i = 0; i < len; ++i) {
        printf("%c ", (pic->data)[i]);
        if ((i+1)%N == 0) printf("\n");
    }
}


void rotatePic(Pic* pic) {
    size_t N = pic->N;
    size_t verticalHalf = N/2;
    size_t horizontalHalf = N/2 + N%2;
    for (size_t i = 0; i < verticalHalf; ++i) {
        for (size_t j = 0; j < horizontalHalf; ++j) {
            size_t x[4]; //indexes by x
            size_t y[4]; //indexes by y
            x[0] = j;
            y[0] = i;
            uint32_t tmp = getPicPixel(pic, x[0], y[0]);
            for (size_t k = 1; k < 4; ++k) {
                x[k] = N - 1 - y[k-1];
                y[k] = x[k - 1];
                uint32_t buf = getPicPixel(pic, x[k], y[k]);
                size_t dataIndex = getPicIndex(pic, x[k], y[k]);
                (*pic).data[dataIndex] = tmp;
                tmp = buf;
            }
            size_t dataIndex = getPicIndex(pic, x[0], y[0]);
            (*pic).data[dataIndex] = tmp;
        }
    }
}


int main() {
    size_t N = 5;
    size_t len = N*N;
    uint32_t arr[len];
    for (size_t i = 0; i < len; ++i) {
        arr[i] = '-';
    }
    Pic pic = {.data = arr, .N = N};
    
    pic.data[getPicIndex(&pic, 1, 4)] = 'A';
    pic.data[getPicIndex(&pic, 1, 3)] = 'B';
    pic.data[getPicIndex(&pic, 1, 2)] = 'C';
    pic.data[getPicIndex(&pic, 1, 1)] = 'D';
    pic.data[getPicIndex(&pic, 2, 4)] = 'N';
    pic.data[getPicIndex(&pic, 3, 4)] = 'M';
    pic.data[getPicIndex(&pic, 2, 3)] = 'K';

    pic.data[getPicIndex(&pic, 0, 0)] = '1';
    pic.data[getPicIndex(&pic, 4, 0)] = '2';
    pic.data[getPicIndex(&pic, 4, 4)] = '3';
    pic.data[getPicIndex(&pic, 0, 4)] = '4';

    printCharPic(&pic);
    rotatePic(&pic);
    printf("\n");
    printCharPic(&pic);
}

